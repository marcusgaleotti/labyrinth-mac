Instructions for Labyrinth:Mac Version  
  
Before you install make sure that you have xcode installed on your machine.  
  
If you do not have xcode installed run, from your bash terminal, : xcode-select --install  
  
1: change your current working directory to where the "Labyrinth Mac" directory is via a terminal session  
2: invoke install.sh  
3: Enjoy it!  

If you want to uninstall the app, invoke the uninstall.sh script included with the game files.

Controls:  
Left-Click to the tile you want to move the player to on the game board.  
Left-Click to the section you want to move the sliding tile into.  
Right-Click to rotate the tile.  
Space to slide the tile in  
[ESC] key to exit the game  
  
This game is in early development and may or may not be worked on in the future.  
  
Terms Of Use:  
The software provided by this repository is offered as is with no guarantees of validity, reliability, or safety and for playing the game which is provided by the software.  
You are welcome to look at and modify the files, but not for  use in any derivations which you will gain monetarily from without written consent from the developers.    
The developers of the software will not be responsible for any damages that arise as a result of installing, playing, or otherwise accessing or downloading the game, though we don't anticipate any issues as there were no such issues during development.  
By downloading and installing this software, you agree to be bound by these terms.  