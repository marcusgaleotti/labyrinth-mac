#ifndef Player_hpp
#define Player_hpp

#include <stdio.h>
#include <cmath>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <iostream>

/*
 Author: Marcus Galeotti
 Purpose: Implementation of player object to represent a player of the game
 Date of Last Update: 10/30/17
 */

class Player
{
    SDL_Rect playerRect = {0};
    SDL_Texture* playerTexture = NULL;
    short int whichPlayer;
    bool turn = false;
    int row = 0, col = 0;
    int tileNo = -1;

public:
    
    Player(SDL_Texture* newTexture = NULL, int playerRectX = 0, int playerRectY = 0, int playerRectWidth = 0, int playerRectHeight = 0, bool turnVal = false, short int whichPlayerVal = 0, int rowVal = 0, int colVal = 0, int tileNoVal = -1)
    {
        playerRect.x = playerRectX;
        playerRect.y = playerRectY;
        playerRect.w = playerRectWidth;
        playerRect.h = playerRectHeight;
        playerTexture = newTexture;
        turn = turnVal;
        whichPlayer = whichPlayerVal;
        row = rowVal;
        col = colVal;
        tileNo = tileNoVal;
            
    }
    SDL_Rect* getRect()
    {
        return &playerRect;
    }
    //temporarily loading the texture myself for development and testing. Will use Rufat's tile loading functions when available
    //returns sdl false if couldn't load texture and sdl true if it could
    SDL_bool setTexture(const char* playerImg, SDL_Renderer* renderer)
    
    {
        SDL_bool success = SDL_FALSE;
        SDL_Texture* texture;
        if((texture = IMG_LoadTexture(renderer, playerImg)) != NULL)
            success = SDL_TRUE;
            
        playerTexture = texture;
        texture = NULL;
        return success;
    }
    void setPlayerVals(const int xVal, const int yVal, const int width, const int height);
    SDL_Texture* getTexture()
    {
        return playerTexture;
    }
    int getPlayerRectX()
    {
        return playerRect.x;
    }
    int getPlayerRectY()
    {
        return playerRect.y;
    }
    void setPlayerRectX(const int x)
    {
        playerRect.x = x;
    }
    void setPlayerRectY(const int y)
    {
        playerRect.y = y;
    }
    void setPlayerRectWidth(const int width)
    {
        playerRect.w = width;
    }
    void setPlayerRectHeight(const int height)
    {
        playerRect.h = height;
    }
    void setPlayerPos(const int xVal, const int yVal, const int tileNoVal, const int boardSize, const int rowVal, const int colVal)//unverified as working
    {
        if(xVal >= 0 && yVal >= 0 && tileNoVal < pow(boardSize, 2) && (rowVal < boardSize) && (colVal < boardSize))
        {
            playerRect.x = xVal;
            playerRect.y = yVal;
            tileNo = tileNoVal;
            row = rowVal;
            col = colVal;
        }
    }
    const bool getTurn()
    {
        return turn;
    }
    void setTurn(const bool turnVal)
    {
        turn = turnVal;
    }
    void setPlayerRow(const int rowVal)
    {
        row = rowVal;
    }
    void setPlayerCol(const int colVal)
    {
        col = colVal;
    }
    int getPlayerCol()
    {
        return col;
    }
    int getPlayerRow()
    {
        return row;
    }
    void setPlayerTile(const int tileNoVal)
    {
        tileNo = tileNoVal;
    }
    int getPlayerTile()
    {
        return tileNo;
    }
    ~Player()
    {
        SDL_DestroyTexture(playerTexture);
        playerTexture = NULL;
        
    }

};

#endif /* Player_hpp */
