
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <exception>
#include "Tile.hpp"
#include "Board.hpp"
#include <unistd.h>


#include "Labyrinth.h"

using namespace std;
//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 640;

int main(int argc, const char * argv[])
{
    
    SDL_Window* window;
    SDL_Renderer* renderer;
    int gameSize = 7;
    int numberOfPlayers = 1;
    try
    {

       /* cout << "Please enter the number of players here -> ";
        cin >> numberOfPlayers;
        
        if(numberOfPlayers <= 0) //we'll deal with correct input later
            numberOfPlayers = 1;
        else if( numberOfPlayers > 4) //force the user into valid choices
            numberOfPlayers = 4;
        else if (numberOfPlayers > 0 && numberOfPlayers <= 4);
        else
        {
            numberOfPlayers = 0;
            cin.ignore(numeric_limits<streamsize>::max(),'\n');
            cin.clear();
        }
        
        cin.ignore(numeric_limits<streamsize>::max(),'\n');
        cin.clear();*/
        
        
        //cin just takes first digit before decimal on floating point if there is one
        
        //get everything all set up in one function
        bool runnable;
        runnable = Board::initSDL(IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_TIF, SDL_INIT_EVERYTHING, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_ALLOW_HIGHDPI | SDL_WINDOW_MOUSE_FOCUS | SDL_WINDOW_INPUT_FOCUS | SDL_WINDOW_MOUSE_FOCUS, &window, &renderer);
        
        Board gameBoard(gameSize, window, renderer, numberOfPlayers);
        SDL_SetWindowIcon(window, IMG_Load("/Users/Shaqcorn/Documents/College/CS370/labyrinth/MarcusLaby/CreateGameBoardWithNewClasses/CreateGameBoardWithNewClasses/pexels-photo-235985.jpeg"));
        gameBoard.render(renderer, true);
        
        while(runnable)
        {
            if((runnable = gameBoard.handleEvents(&gameBoard, window, renderer)))
            {
                gameBoard.render(renderer, true);
                
            }
        }
            
        
    }
    catch(runtime_error a)
    {
        Board::displaySDLError(std::cerr, a.what());
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
       // Mix_CloseAudio();
        TTF_Quit();
        IMG_Quit();
        
        SDL_Quit();
        return EXIT_FAILURE;
    }
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    IMG_Quit();
    
    SDL_Quit();
}

