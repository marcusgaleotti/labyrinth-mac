#include "Board.hpp"
/*
 Author: Marcus Galeotti
 Purpose: Board class that represents the whole game board. To be used with a tile and player class.
 Date of last update: 11/27/2017
 */
Board::Board(const int sizeOfBoard,  SDL_Window* window, SDL_Renderer* renderer, const int numberOfPlayers, const bool firstInitVal)
{
    using namespace std;

    size = sizeOfBoard;
    tiles.resize(size);
    numOfPlayers = numberOfPlayers;
    players.resize(numberOfPlayers);
    
    string basePath = "/Users/Shared/Labyrinth/players/p";
    stringstream sstr;
    sstr << basePath;

    for(int i = 0; i < tiles.size(); i++)
        tiles.at(i).resize(size);
    
    for(int i = 0; i < numberOfPlayers; i++)
    {
        sstr << (i + 1) << ".png";
        if(players.at(i).setTexture(sstr.str().c_str(), renderer) == SDL_FALSE)
            throw runtime_error("IMG_Load failed: ");
       //debugging output
        sstr.str("");
               // cerr << sstr.str() << endl << endl;
        sstr << basePath;
                //cerr << sstr.str() << endl << endl;
    }
    
    buffer.setTexture("/Users/Shared/Labyrinth/tiles/1.jpg",renderer);
    this -> setTexture("/Users/Shared/Labyrinth/bg/bg.jpeg", renderer);
   if(numberOfPlayers > 0)
    players.at(0).setTurn(true);
    
    firstInit = firstInitVal;
    initGameBoard(window, renderer);
    firstInit = false;
    
}

SDL_bool Board::setTexture(const char* tileImg, SDL_Renderer* renderer)//temporary measure
{
    
    SDL_bool success = SDL_FALSE;
    SDL_Texture* texture;
    if((texture = IMG_LoadTexture(renderer, tileImg)) != NULL)
        success = SDL_TRUE;
    
    boardTexture = texture;
    texture = NULL;
    return success;
}
void Board::render(SDL_Renderer* renderer,  const bool renderingTextures, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip)
{
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255); //set the "background" color of the window
    //to the rgba value specified
    SDL_RenderClear(renderer); //clear the current rendering target with the draw color
    
    if(renderingTextures)
    {
        SDL_RenderCopy(renderer, this -> boardTexture , NULL, NULL);
        SDL_RenderCopyEx(renderer, buffer.getTexture(), clip, buffer.getRect(),  buffer.getAngle(), center, flip);
        if(buffer.getTreasureTexture() != NULL)
        {
            SDL_SetTextureBlendMode(buffer.getTreasureTexture(), SDL_BLENDMODE_BLEND);
            SDL_SetTextureAlphaMod(buffer.getTreasureTexture(), 200);
            SDL_RenderCopy(renderer, buffer.getTreasureTexture(), NULL, buffer.getRect());
        }
        
        for(int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                
                SDL_RenderCopyEx(renderer, tiles.at(i).at(j).getTexture(), clip, tiles.at(i).at(j).getRect(), tiles.at(i).at(j).getAngle(), center, flip);
                SDL_SetTextureBlendMode(tiles.at(i).at(j).getTreasureTexture(), SDL_BLENDMODE_BLEND);
                SDL_SetTextureAlphaMod(tiles.at(i).at(j).getTreasureTexture(), 200);
                SDL_RenderCopy(renderer, tiles.at(i).at(j).getTreasureTexture(), NULL, tiles.at(i).at(j).getRect());
                
            }
        }
    }
    else
    {
        SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND); //set blending mode to allow for opacity
        //with no intentional change in colors
        SDL_SetRenderDrawColor(renderer, 250, 70, 80, 200); //set incremental, random
        //colors
        SDL_RenderFillRect(renderer, buffer.getRect());
        SDL_RenderDrawRect(renderer, buffer.getRect());
        for(int i = 0; i < size; i++) //fill rectangles and copy them to the display buffer
        {
            for (int j = 0; j < size; j++)
            {
                
                SDL_SetRenderDrawColor(renderer, 250, 70, 80, 200); //set incremental, random
                //colors
                SDL_RenderFillRect(renderer,tiles.at(i).at(j).getRect()); //set rectangle color to renderdrawcolor
                SDL_RenderDrawRect(renderer, tiles.at(i).at(j).getRect()); //copy rect to rendering buffer
            }
        }
    }
    for(int i = 0; i < players.size(); i++)
    {
        SDL_SetTextureBlendMode(players.at(i).getTexture(), SDL_BLENDMODE_BLEND);
        SDL_SetTextureAlphaMod(players.at(i).getTexture(), 200);
        SDL_RenderCopy(renderer, players.at(i).getTexture(), NULL, players.at(i).getRect());
    }
    
    SDL_RenderPresent(renderer);
}


void Board::initGameBoard(SDL_Window* window, SDL_Renderer* renderer)
{
    using namespace std;
    int winWidthPixels, winHeightPixels;
    const int ROWS = this->size; //rows and cols are same on an NxN board
    const int COLS = this->size;
    int straightTiles = straight;
    int cornerUnfixedTreas = cornerUnfixedWithTresr;
    int cornerUnfixedNoTreas = cornerUnfixedNoTresr - 1; //last one will be the first buffer/storage tile used for sliding
    int teeUnfixedwithTreasure = this -> TeeUnfixedWithTreasr;
    int randomizer = 0;
    
    SDL_GL_GetDrawableSize(window, &winWidthPixels, &winHeightPixels); //initialize window width and
                                                                 //height with provided ints
    int offsetX = 0, rectW = 0; //offsets allow a centered board
    int offsetY = 0, rectH = 0;
    
    switch(COLS) //computing tile width based on current, actual window size
    {
        case 3:
            offsetX = rectW  = (winWidthPixels / COLS) /1.7; //numbers are very specific but IMPORTANT
            break;
        case 5:
            offsetX = rectW  = (winWidthPixels / COLS) /1.4;
            break;
        case 7:
            offsetX = rectW  = (winWidthPixels / COLS) /1.275;
            break;
        case 9:
            offsetX = rectW  = (winWidthPixels / COLS) /1.23;
            break;
        case 11:
            offsetX = rectW = (winWidthPixels / COLS)/ 1.17;
            break;
        default:
            offsetX = rectW = (winWidthPixels / COLS);
            break;
    }
    
    switch(ROWS) //computing tile width based on current, actual window size
    {
        case 3:
            offsetY = rectH  = (winHeightPixels / ROWS) /1.65; //numbers are very specific but IMPORTANT
            break;
        case 5:
            offsetY = rectH  = (winHeightPixels / ROWS) /1.45;
            break;
        case 7:
            offsetY = rectH  = (winHeightPixels / ROWS) /1.27;
            break;
        case 9:
            offsetY = rectH  = (winHeightPixels / ROWS) /1.22;
            break;
        case 11:
            offsetY = rectH  = (winHeightPixels / ROWS) /1.17;
            break;
            
        default:
            offsetY = rectH  = (winHeightPixels / ROWS);
            break;
    }
    
    this -> offsetX = offsetX;
    this -> offsetY = offsetY;
    
    buffer.getRect() -> w = rectW;
    buffer.getRect() -> h = rectH;
    for(int i = 0; i < ROWS; i++) //initializing tiles' SDL_Rect's appropriate values based on window size
    {
        for(int j = 0; j < COLS; j++)
        {
            tiles.at(i).at(j).getRect()->w = rectW; //setting TILE width and height
            tiles.at(i).at(j).getRect()->h = rectH;
            
            
            tiles.at(i).at(j).getRect()->y = offsetY + (offsetY * i); //setting TILE board positions
            tiles.at(i).at(j).getRect()->x = offsetX + (offsetX * j);
            tiles.at(i).at(j).setRow(i);
            tiles.at(i).at(j).setCol(j);
            
           
            
                //debugging output
               // cerr << boolalpha << endl << "i: " << i << " j: " << j << endl << " top: " << tiles.at(i).at(j).getTop() << " bottom: " << tiles.at(i).at(j).getBot() << " left: " << tiles.at(i).at(j).getLeft() << " right: " << tiles.at(i).at(j).getRight() << endl;
                
        }
    }
    
    if(!firstInit)
    {
        
        if(numOfPlayers > 0)
        {
            for(int i = 0; i < ROWS; i++)
            {
                for(int j = 0; j < COLS; j++)
                {
                    if(tiles.at(i).at(j).getHasPlayer())
                    {
                        switch(tiles.at(i).at(j).getWhichPlayer())
                        {
                            case 0:
                                players.at(0).setPlayerVals(tiles.at(i).at(j).getTileX(), tiles.at(i).at(j).getTileY(), rectW, rectH);
                                players.at(0).setPlayerRow(i);
                                players.at(0).setPlayerCol(j);
                                break;
                            case 1:
                                players.at(1).setPlayerVals(tiles.at(i).at(j).getTileX(), tiles.at(i).at(j).getTileY(), rectW, rectH);
                                players.at(1).setPlayerRow(i);
                                players.at(1).setPlayerCol(j);
                                break;
                            case 2:
                                players.at(2).setPlayerVals(tiles.at(i).at(j).getTileX(), tiles.at(i).at(j).getTileY(), rectW, rectH);
                                players.at(2).setPlayerRow(i);
                                players.at(2).setPlayerCol(j);
                                break;
                            case 3:
                                players.at(3).setPlayerVals(tiles.at(i).at(j).getTileX(), tiles.at(i).at(j).getTileY(), rectW, rectH);
                                players.at(3).setPlayerRow(i);
                                players.at(3).setPlayerCol(j);
                                break;
                            default:
                                break;
                        }
                    }
                }
                snapBufferToMouseGrid(true, NULL);
            }
        }
    }
    else
    {
        buffer.getRect() -> x = offsetX; //initializing sliding buffer tile for first time
        buffer.getRect() -> y = 0;
        buffer.getRect() -> w = rectW;
        buffer.getRect() -> h = rectH;
        tiles.at(0).at(0).addBuffer(&buffer);
        tiles.at(0).at(0).setBufferTop(true);
        srand(static_cast<unsigned int>(time(NULL))); //initialize the random number generator for randomizing board
        
        for(int i = 0; i < tiles.size(); i++)
        {
            for(int j = 0; j < tiles.at(i).size(); j++)
            {
                if(((i % 2) == 0) && ((j % 2) == 0)) //just for 7x7 board. Not generalized. only for fixed tiles
                {
                    tiles.at(i).at(j).setFix(true);
                    
                    if(i == 0 && j == 0) //top left corner
                    {
                        tiles.at(i).at(j).setLeft(false);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTop(false);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/2.jpg", renderer);
                        tiles.at(i).at(j).setAngle(0);
                    }
                    else if (i == 0 && j == (COLS - 1)) //top right corner
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(false);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTop(false);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/2.jpg", renderer);
                        tiles.at(i).at(j).setAngle(90);
                    }
                    else if (i == (ROWS - 1) && j == 0) //bottom left corner
                    {
                        tiles.at(i).at(j).setLeft(false);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(false);
                        tiles.at(i).at(j).setTop(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/2.jpg", renderer);
                        tiles.at(i).at(j).setAngle(270);
                    }
                    else if (i == ROWS - 1 && j == COLS - 1) //bottom right corner
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(false);
                        tiles.at(i).at(j).setBot(false);
                        tiles.at(i).at(j).setTop(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/2.jpg", renderer);
                        tiles.at(i).at(j).setAngle(180);
                    }
                    else if (i == ROWS - 1 && ((j == 2) || (j == 4))) //bottom row middle 2
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(false);
                        tiles.at(i).at(j).setTop(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setAngle(180);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/4.png", renderer);
                    }
                    else if(i == 0 && ((j == 2) || (j == 4))) // top row middle 2
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTop(false);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setAngle(0);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/18.png", renderer);
                    }
                    else if (((i == 2) || (i == 4)) && j == 0) //middle two on left side
                    {
                        tiles.at(i).at(j).setLeft(false);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTop(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setAngle(270);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/3.png", renderer);
                    }
                    else if (((i == 2) || (i == 4)) && j == COLS - 1) //middle two on right side
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(false);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTop(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setAngle(90);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/2.png", renderer);
                    }
                    else if (i == 2 && j == 2) // tile at (2,2)
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTop(false);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setAngle(270);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/5.png", renderer);
                    }
                    else if (i == 2 && j == 4) //tile at (2,4)
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(false);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTop(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setAngle(0);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/6.png", renderer);
                    }
                    else if (i == 4 && j == 2) //tile at (4,2)
                    {
                        tiles.at(i).at(j).setLeft(false);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTop(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setAngle(180);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/8.png", renderer);
                    }
                    else if (i == 4 && j == 4) //tile at (4,4)
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(false);
                        tiles.at(i).at(j).setTop(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setAngle(90);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/9.png", renderer);
                    }
                    
                }
                else //need to finish this clause, update all the parts
                {
                    randomizer = rand() % 100; //getting a uniform set
                    if(straightTiles != 0 && (randomizer <= 24))
                    {
                        tiles.at(i).at(j).setLeft(true); //these values don't match angles
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/1.jpg", renderer);
                        tiles.at(i).at(j).setAngle((90 * i) % 360);
                        
                        straightTiles--;
                    }
                    else if(cornerUnfixedTreas != 0 && (randomizer <= 49 && randomizer >= 25))
                    {
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/2.jpg", renderer);
                        tiles.at(i).at(j).setAngle((90 * i) % 360);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/21.png", renderer);
                        //add treasure char
                        cornerUnfixedTreas--;
                        
                    }
                    else if(cornerUnfixedNoTreas != 0 && (randomizer <= 74 && randomizer >= 50))
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/2.jpg", renderer);
                        cornerUnfixedNoTreas--;
                    }
                    else if(teeUnfixedwithTreasure != 0 && (randomizer <= 99 && randomizer >= 75))
                    {
                        tiles.at(i).at(j).setLeft(true);
                        tiles.at(i).at(j).setRight(true);
                        tiles.at(i).at(j).setBot(true);
                        tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                        tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/15.png", renderer);
                        teeUnfixedwithTreasure--;
                    }
                    else
                    {
                        if(straightTiles != 0)
                        {
                            tiles.at(i).at(j).setLeft(true);
                            tiles.at(i).at(j).setRight(true);
                            tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/1.jpg", renderer);
                            tiles.at(i).at(j).setAngle((90 * i) % 360);
                            
                            straightTiles--;
                        }
                        else if(cornerUnfixedTreas != 0)
                        {
                            tiles.at(i).at(j).setBot(true);
                            tiles.at(i).at(j).setRight(true);
                            tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/2.jpg", renderer);
                            tiles.at(i).at(j).setAngle((90 * i) % 360);
                            tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/8.png", renderer);
                            //add treasure char
                            cornerUnfixedTreas--;
                            
                        }
                        else if(cornerUnfixedNoTreas != 0)
                        {
                            tiles.at(i).at(j).setLeft(true);
                            tiles.at(i).at(j).setRight(true);
                            tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/2.jpg", renderer);
                            cornerUnfixedNoTreas--;
                        }
                        else if(teeUnfixedwithTreasure != 0)
                        {
                            tiles.at(i).at(j).setLeft(true);
                            tiles.at(i).at(j).setRight(true);
                            tiles.at(i).at(j).setBot(true);
                            tiles.at(i).at(j).setTexture("/Users/Shared/Labyrinth/tiles/3.jpg", renderer);
                            tiles.at(i).at(j).setTreasureTexture("/Users/Shared/Labyrinth/treasure/6.png", renderer);
                            teeUnfixedwithTreasure--;
                        }
                        
                    }
                }
            }
        }
        if(numOfPlayers > 0)
        {
            const int tileCols = COLS - 1, tileRows = ROWS - 1;
            //setting up starting game board
            for(int i = 0; i < numOfPlayers; i++)
            {
                switch(i)
                {
                    case 0:
                        tiles.at(0).at(0).setPlayer(0);
                        players.at(0).setPlayerVals(tiles.at(0).at(0).getTileX(), tiles.at(0).at(0).getTileY(), rectW, rectH);
                        break;
                    case 1:
                    {
                        tiles.at(0).at(tileCols).setPlayer(1);
                        players.at(1).setPlayerVals(tiles.at(0).at(tileCols).getTileX(), tiles.at(0).at(tileCols).getTileY(), rectW, rectH);
                    }
                        break;
                    case 2:
                        tiles.at(tileRows).at(tileCols).setPlayer(2);
                        players.at(2).setPlayerVals(tiles.at(tileRows).at(tileCols).getTileX(), tiles.at(tileRows).at(tileCols).getTileY(), rectW, rectH);
                        break;
                    case 3:
                        tiles.at(tileRows).at(0).setPlayer(3);
                        players.at(3).setPlayerVals(tiles.at(tileRows).at(0).getTileX(), tiles.at(tileRows).at(0).getTileY(), rectW, rectH);
                        break;
                    default:
                        break;
                }
            }
            
        }
    }
}


/* this function throws runtime_error on error initialized with a string value to determine what error caused the exception to be thrown */
bool Board::initSDL(int IMG_INIT_FLAGS, Uint32 SDL_INIT_FLAGS, int width, int height, Uint32 window_flags, SDL_Window** window, SDL_Renderer** renderer)
{
    SDL_SetHint(SDL_HINT_VIDEO_ALLOW_SCREENSAVER, "1");
   // SDL_SetHint(SDL_HINT_RENDER_VSYNC, "1");
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1");
        if(IMG_Init(IMG_INIT_FLAGS) != (IMG_INIT_FLAGS))
        {
            throw std::runtime_error("IMG_init error: ");
        }
        else if(TTF_Init() == -1)
        {
            throw std::runtime_error("TFF_Init error: ");
        }
        else if(SDL_Init(SDL_INIT_FLAGS) != 0)
        {
            throw std::runtime_error("SDL_Init error: ");
        }
        else if(SDL_CreateWindowAndRenderer(width, height, window_flags, window, renderer) != 0)
        {
            throw std::runtime_error("Failed to create window or renderer:");
        }
       /* else if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 60000) == -1)
        {
            throw std::runtime_error("Failed to init SDL_Mixer: ");
        }*/
        else
        {
            SDL_SetWindowTitle(*window, "Labyrinth");
            return true;
        }
}
void Board::displaySDLError(std::ostream &os, const std::string &msg)
{
    
    if(msg == "IMG_Load failed: ")
        os << msg << IMG_GetError() << std::endl;
    else
        os << msg << SDL_GetError() << std::endl;
}
int Board::handleEvents(Board* gameBoard, SDL_Window* window, SDL_Renderer* renderer)
{
    using namespace std;
    SDL_Event sdlEvent = {0};
    SDL_WaitEvent(&sdlEvent);
    bool runnable = true;
        switch(sdlEvent.type)
        {
            case SDL_KEYUP:
                if(sdlEvent.key.keysym.sym == SDLK_ESCAPE)
                    runnable = false;
                else if(sdlEvent.key.keysym.sym == SDLK_SPACE)
                    slide(renderer);
                break;
            case SDL_WINDOWEVENT:
                switch(sdlEvent.window.event)
            {
                case SDL_WINDOWEVENT_SIZE_CHANGED:
                    gameBoard->initGameBoard(window);
                    SDL_SetWindowInputFocus(window);
                    break;
                case SDL_WINDOWEVENT_CLOSE:
                    runnable = false;
                    break;
                case SDL_WINDOWEVENT_MINIMIZED:
                    SDL_MinimizeWindow(window);
                    break;
                case SDL_WINDOWEVENT_MAXIMIZED:
                    gameBoard->initGameBoard(window);
                    SDL_SetWindowInputFocus(window);
                    break;
                case SDL_WINDOWEVENT_RESTORED:
                    SDL_RestoreWindow(window);
                    SDL_SetWindowInputFocus(window);
                    gameBoard->initGameBoard(window);
                    break;
                case SDL_WINDOWEVENT_TAKE_FOCUS:
                    SDL_SetWindowInputFocus(window);
                    SDL_RestoreWindow(window);
                    break;
                case SDL_WINDOWEVENT_EXPOSED:
                    SDL_RestoreWindow(window);
                    SDL_SetWindowInputFocus(window);
                    gameBoard->initGameBoard(window);
                    break;
                default:
                     // std::cerr << "window event as number: " << sdlEvent.window.event<< std::endl;
                    break;
            }
                break;
            case SDL_MOUSEBUTTONUP:
                    if(sdlEvent.button.button == SDL_BUTTON_RIGHT)
                    {
                        buffer.rotate();
                    }
                    else if(checkIfClicked(sdlEvent) != -1)
                    {
                        if(numOfPlayers > 0)
                            if(sdlEvent.button.button == SDL_BUTTON_LEFT)
                                movePlayer(true);
                        
                    }
                    else
                    {
                        if(sdlEvent.button.button == SDL_BUTTON_LEFT)
                            snapBufferToMouseGrid(false, &sdlEvent);
                    }
                break;
            default:
               // std::cerr << "event type as number: " << sdlEvent.type << std::endl;
                break;
        }
    return runnable;
}

int Board::checkIfClicked(const SDL_Event sdlEvent) //updating of tileNoRowCol needs testing
{
    const int ROWS = this->size;
    const int COLS = this->size;
    int tileNumberCount = -1;
    bool aTileClicked = false;
    std::stringstream tileNo;
    
    for (int i = 0; i < ROWS; i++)
        for(int j = 0; j < COLS; j++) //all text output to screen is for testing and
                                                        //debugging purposes only
        {
            tiles.at(i).at(j).setFocus(false); //resets focus var in all tiles. If one gets clicked,
                                                //its focus is set to true below. It remains true until this
                                                //function is called again (another click event occurs)
                                                //need to iterate over all tiles to ensure they are all
                                                //reset, so can't make this more efficient that way
            tiles.at(i).at(j).setFocusButton(0); //set to zero by default in the class to begin, so setting it back to that
            tileNumberCount++;
            switch(clicked(sdlEvent, tiles.at(i).at(j).getRect()))
            {
                    
                case SDL_BUTTON_LEFT:
                    tiles.at(i).at(j).setFocus(true);
                    
                   // std::cerr << std::boolalpha << "left mouse button clicked " << tiles.at(i).at(j).getFocus() <<  std::endl;
                    tileNo << "the tile clicked is tile number " << tileNumberCount;
                   // std::cerr << tileNo.str() << std::endl;
                    aTileClicked = true;
                    tiles.at(i).at(j).setFocusButton(SDL_BUTTON_LEFT);
                    break;
                case SDL_BUTTON_MIDDLE:
                    tiles.at(i).at(j).setFocus(true);
                    // std::cerr <<  std::boolalpha << "middle mouse button clicked " << tiles.at(i).at(j).getFocus() << std::endl;
                    tileNo << "the tile clicked is tile number " << tileNumberCount;
                  //  std::cerr << tileNo.str() << std::endl;
                    aTileClicked = true;
                    tiles.at(i).at(j).setFocusButton(SDL_BUTTON_MIDDLE);
                    break;
                case SDL_BUTTON_RIGHT:
                    tiles.at(i).at(j).setFocus(true);
                  //  std::cerr << std::boolalpha << "right mouse button clicked " << tiles.at(i).at(j).getFocus() << std::endl;
                    tileNo << "the tile clicked is tile number " << tileNumberCount;
                 //   std::cerr << tileNo.str() << std::endl;
                    aTileClicked = true;
                    tiles.at(i).at(j).setFocusButton(SDL_BUTTON_RIGHT);
                    break;
                
                default:
                  //  std::cerr << std::boolalpha << "Tile at: (" << tiles.at(i).at(j).getTileX() << "," << tiles.at(i).at(j).getTileY() << ")"<< " not clicked " << tiles.at(i).at(j).getFocus() << std::endl;
                    break;
            }
           // std::cerr << std::endl;
        }
    
    if(aTileClicked)
        return tileNumberCount;
    else
        return -1;    
}
short int Board::clicked(const SDL_Event sdlEvent, const SDL_Rect* tileRect)
{
    using namespace std;
    int returnVal = -999;
    SDL_Point mouseClickPoint;
    
    mouseClickPoint.x = sdlEvent.motion.x;
    mouseClickPoint.y = sdlEvent.motion.y;

    //debug
  //  cout << "Mouse position: (" << mouseClickPoint.x << "," << mouseClickPoint.y << ")" << endl;
    
    if(tileRect == NULL)
        returnVal = -3;
    else if(SDL_PointInRect(&mouseClickPoint, tileRect) == SDL_TRUE)
        switch(sdlEvent.button.button)
        {
            case SDL_BUTTON_LEFT:
                 if(sdlEvent.button.state == SDL_RELEASED)
                 {
                     returnVal = SDL_BUTTON_LEFT;
                   //  std::cout << "left button " << std:: endl << "times clicked: " << sdlEvent.button.clicks << endl;
                 }
                break;
            case SDL_BUTTON_MIDDLE:
                if(sdlEvent.button.state == SDL_RELEASED)
                {
                    returnVal = SDL_BUTTON_MIDDLE;
                   // cout << "middle button" << endl;
                }
                break;
            case SDL_BUTTON_RIGHT:
                 if(sdlEvent.button.state == SDL_RELEASED)
                 {
                     returnVal = SDL_BUTTON_RIGHT;
                    // cout << "right button" << endl;
                 }
                break;
                
            default:
                returnVal = -2;
                break;
        }
    else
        returnVal = -1;
    
    return returnVal;
}
void Board::movePlayer(const bool clicking) //needs testing
{
    using namespace std;
    //if it is this player's turn
    int hasTurn = -1; // variable to track and set next player's turn
    int tileNo = -1; //track the tile a player is on to be stored in player's object
    for (int i = 0 ; i < tiles.size(); i++) //check each tile if it was clicked. Kind of inefficient, but it works
    {
        for (int j = 0; j < tiles.at(i).size(); j++)
        {
           // tiles.at(i).at(j).removePlayer();
            tileNo++;
            if(tiles.at(i).at(j).getFocus())
            {
                tiles.at(i).at(j).setFocus(false);
                if(clicking)
                {
                    for(int k = 0; k < numOfPlayers; k++)
                    {
                        if(players.at(k).getTurn())
                        {
                                hasTurn = k;
                                tiles.at(i).at(j).removeTreasure();
                                tiles.at(players.at(k).getPlayerRow()).at(players.at(k).getPlayerCol()).removePlayer();
                                players.at(k).setPlayerPos(tiles.at(i).at(j).getTileX(), tiles.at(i).at(j).getTileY(), tileNo, this -> size, i, j);
                            
                                tiles.at(i).at(j).setPlayer(k);
                        }
                    }
                }
                else
                {
                    for(int k = 0; k < numOfPlayers; k++)
                    {
                        if(players.at(k).getTurn())
                        {
                            hasTurn = k;
                            tiles.at(players.at(k).getPlayerRow()).at(players.at(k).getPlayerCol()).removePlayer();
                            players.at(k).setPlayerPos(tiles.at(i).at(j).getTileX(), tiles.at(i).at(j).getTileY(), tileNo, this -> size, i, j);
                            
                            tiles.at(i).at(j).setPlayer(k);
                        }
                    }
                }
            }
        }
    }
    if(numOfPlayers > 0 && numOfPlayers < 5 && hasTurn != -1 && !clicking) //this part just cycles through the players and gives each one a turn
    {
        players.at(hasTurn).setTurn(SDL_FALSE);
        players.at((hasTurn + 1) % numOfPlayers).setTurn(SDL_TRUE);
    }
}
void Board::slide(SDL_Renderer* renderer)
{
    using namespace std;
    bool foundAdjacent = false;
    for(int ROW = 0; ROW < tiles.size(); ROW++)
    {
        if(foundAdjacent)
        {
            ROW = tiles.size();
            continue;
        }
        for(int COL = 0; COL < tiles.at(ROW).size(); COL++)
        {
            if(foundAdjacent)
            {
                COL = tiles.at(ROW).size();
                continue;
            }
            if(!(tiles.at(ROW).at(COL).getFixed()))
            {
                if(tiles.at(ROW).at(COL).getBufferIsAdjacent())
                {
                    foundAdjacent = true;
                    if(tiles.at(ROW).at(COL).getBufferTop()) //sliding from above
                    {
                        slideFromAbove(ROW, COL, this -> size, renderer);
                    }
                    else if(tiles.at(ROW).at(COL).getBufferLeft()) //sliding right ->
                    {
                        slideFromLeft(ROW, COL, this -> size, renderer);
                    }
                    else if(tiles.at(ROW).at(COL).getBufferRight()) // <- sliding left
                    {
                        slideFromRight(ROW, COL, this -> size, renderer);
                    }
                    else if(tiles.at(ROW).at(COL).getBufferBottom())
                    {
                        slideFromBottom(ROW, COL, this -> size, renderer);
                    }
                }
            }
            
        }
    }
    
}
void Board::slideFromBottom(const int ROW, const int COL, const int size, SDL_Renderer* renderer)
{
    for(int i = 0; i < size - 1; i++)
    {
        if(i == 0) //setting buffer, storage and last tile
        {
            storage = buffer;
            
            buffer = tiles.at(0).at(COL);
            tiles.at(0).at(COL).removeBuffer();
            tiles.at(0).at(COL).addBuffer(&buffer);
            tiles.at(0).at(COL).setBufferTop(true);
            buffer.setTileY(tiles.at(0).at(COL).getTileY() - offsetY);
            buffer.setAngle(tiles.at(0).at(COL).getAngle());
            tiles.at(ROW).at(COL).removeBuffer();
            
            render(renderer, true);
        }
        
        tiles.at(i).at(COL).setBot(tiles.at(i + 1).at(COL).getBot()); //setting tiles inbetween first and last
        tiles.at(i).at(COL).setTop(tiles.at(i + 1).at(COL).getTop());
        tiles.at(i).at(COL).setLeft(tiles.at(i + 1).at(COL).getLeft());
        tiles.at(i).at(COL).setRight(tiles.at(i + 1).at(COL).getRight());
        tiles.at(i).at(COL).setTexture(tiles.at(i + 1).at(COL).getTileTexStr().c_str(), renderer);
        tiles.at(i).at(COL).setAngle(tiles.at(i + 1).at(COL).getAngle());
        
        if(tiles.at(i + 1).at(COL).getTresTexStr() == "")
            tiles.at(i).at(COL).setTreasureTexture(NULL, renderer);
        else
            tiles.at(i).at(COL).setTreasureTexture(tiles.at(i + 1).at(COL).getTresTexStr().c_str(), renderer);
        
        render(renderer, true);
        if(i == size - 2) //setting first tile to storage
        {
            tiles.at(size - 1).at(COL).setBot(storage.getBot());
            tiles.at(size - 1).at(COL).setTop(storage.getTop());
            tiles.at(size - 1).at(COL).setLeft(storage.getLeft());
            tiles.at(size - 1).at(COL).setRight(storage.getRight());
            tiles.at(size - 1).at(COL).setTexture(storage.getTileTexStr().c_str(), renderer);
            tiles.at(size - 1).at(COL).setTreasureTexture(storage.getTresTexStr().c_str(), renderer);
            tiles.at(size - 1).at(COL).setAngle(storage.getAngle());
            
        }
        
        
        
        render(renderer, true);
    }
    if(tiles.at(0).at(COL).getHasPlayer()) //moving player(s) setting player to opposite side of board
    {
        tiles.at(0).at(COL).removePlayer();
        tiles.at(size - 1).at(COL).setFocus(true);
        movePlayer(false);
        
    }
    else if(tiles.at(size - 1).at(COL).getHasPlayer()) //moving player from first tile to second
    {
        tiles.at(size - 1).at(COL).removePlayer();
        tiles.at(size - 2).at(COL).setFocus(true);
        movePlayer(false);
    }
    else
    {
        for(int i = 0; i < size - 1; i++)
        {
            if(tiles.at(i).at(COL).getHasPlayer())//need to work on for players on the same column
            {
                tiles.at(i).at(COL).removePlayer();
                tiles.at(i - 1).at(COL).setFocus(true);
                movePlayer(false);
            }
        }
    }
}
void Board::slideFromRight(const int ROW, const int COL, const int size, SDL_Renderer* renderer)
{
    for(int i = 0; i < size - 1; i++)
    {
        
        if(i == 0) //setting buffer, storage and last tile
        {
            storage = buffer;
            
            buffer = tiles.at(ROW).at(0);
            tiles.at(ROW).at(0).removeBuffer(); //just makes sure buffer values are reset
            tiles.at(ROW).at(0).addBuffer(&buffer);
            tiles.at(ROW).at(0).setBufferLeft(true);
            buffer.setTileX(tiles.at(ROW).at(0).getTileX() - offsetX);
            buffer.setAngle(tiles.at(ROW).at(0).getAngle());
            tiles.at(ROW).at(COL).removeBuffer();
            
        }
        
        tiles.at(ROW).at(i).setBot(tiles.at(ROW).at(i + 1).getBot()); //setting tiles inbetween first and last
        tiles.at(ROW).at(i).setTop(tiles.at(ROW).at(i + 1).getTop());
        tiles.at(ROW).at(i).setLeft(tiles.at(ROW).at(i + 1).getLeft());
        tiles.at(ROW).at(i).setRight(tiles.at(ROW).at(i + 1).getRight());
        tiles.at(ROW).at(i).setTexture(tiles.at(ROW).at(i + 1).getTileTexStr().c_str(), renderer);
        tiles.at(ROW).at(i).setAngle(tiles.at(ROW).at(i + 1).getAngle());
        
        if(tiles.at(ROW).at(i + 1).getTresTexStr() == "")
            tiles.at(ROW).at(i).setTreasureTexture(NULL, renderer);
        else
            tiles.at(ROW).at(i).setTreasureTexture(tiles.at(ROW).at(i + 1).getTresTexStr().c_str(), renderer);
        
        if(i == size - 2) //setting first tile to storage
        {
            tiles.at(ROW).at(size - 1).setBot(storage.getBot());
            tiles.at(ROW).at(size - 1).setTop(storage.getTop());
            tiles.at(ROW).at(size - 1).setLeft(storage.getLeft());
            tiles.at(ROW).at(size - 1).setRight(storage.getRight());
            tiles.at(ROW).at(size - 1).setTexture(storage.getTileTexStr().c_str(), renderer);
            tiles.at(ROW).at(size - 1).setTreasureTexture(storage.getTresTexStr().c_str(), renderer);
            tiles.at(ROW).at(size - 1).setAngle(storage.getAngle());
            
        }
        render(renderer, true);
    }
    if(tiles.at(ROW).at(0).getHasPlayer()) //moving player(s)
    {
        tiles.at(ROW).at(0).removePlayer();
        tiles.at(ROW).at(size - 1).setFocus(true);
        movePlayer(false);
        
    }
    else if(tiles.at(ROW).at(size - 1).getHasPlayer())
    {
        tiles.at(ROW).at(size - 1).removePlayer();
        tiles.at(ROW).at(size - 2).setFocus(true);
        movePlayer(false);
        
    }
    else
    {
        for(int i = 0; i < size - 1; i++)
        {
            if(tiles.at(ROW).at(i).getHasPlayer())//need to work on for players on the same column
            {
                tiles.at(ROW).at(i).removePlayer();
                tiles.at(ROW).at(i - 1).setFocus(true);
                movePlayer(false);
            }
        }
    }
}
void Board::slideFromLeft(const int ROW, const int COL, const int size, SDL_Renderer* renderer)
{
    for(int i = size - 1; i >= 1; i--)
    {
        
        if(i == size - 1) //setting buffer, storage and last tile
        {
            storage = buffer;
            
            buffer = tiles.at(ROW).at(size - 1);
            tiles.at(ROW).at(size - 1).removeBuffer();
            tiles.at(ROW).at(size - 1).addBuffer(&buffer);
            tiles.at(ROW).at(size - 1).setBufferRight(true);
            buffer.setTileX(tiles.at(ROW).at(size - 1).getTileX() + offsetX);
            buffer.setAngle(tiles.at(ROW).at(size - 1).getAngle());
            tiles.at(ROW).at(COL).removeBuffer();
            
        }
        
        
        tiles.at(ROW).at(i).setBot(tiles.at(ROW).at(i - 1).getBot()); //setting tiles inbetween first and last
        tiles.at(ROW).at(i).setTop(tiles.at(ROW).at(i - 1).getTop());
        tiles.at(ROW).at(i).setLeft(tiles.at(ROW).at(i - 1).getLeft());
        tiles.at(ROW).at(i).setRight(tiles.at(ROW).at(i - 1).getRight());
        tiles.at(ROW).at(i).setTexture(tiles.at(ROW).at(i - 1).getTileTexStr().c_str(), renderer);
        tiles.at(ROW).at(i).setAngle(tiles.at(ROW).at(i - 1).getAngle());
        
        if(tiles.at(ROW).at(i - 1).getTresTexStr() == "")
            tiles.at(ROW).at(i).setTreasureTexture(NULL, renderer);
        else
            tiles.at(ROW).at(i).setTreasureTexture(tiles.at(ROW).at(i - 1).getTresTexStr().c_str(), renderer);
        
        if(i == 1) //setting first tile to storage
        {
            tiles.at(ROW).at(0).setBot(storage.getBot());
            tiles.at(ROW).at(0).setTop(storage.getTop());
            tiles.at(ROW).at(0).setLeft(storage.getLeft());
            tiles.at(ROW).at(0).setRight(storage.getRight());
            tiles.at(ROW).at(0).setTexture(storage.getTileTexStr().c_str(), renderer);
            tiles.at(ROW).at(0).setTreasureTexture(storage.getTresTexStr().c_str(), renderer);
            tiles.at(ROW).at(0).setAngle(storage.getAngle());
            
        }
        render(renderer, true);
    }
    if(tiles.at(ROW).at(size - 1).getHasPlayer()) //moving player(s)
    {
        tiles.at(ROW).at(size - 1).removePlayer();
        tiles.at(ROW).at(0).setFocus(true);
        movePlayer(false);
        
    }
    else if(tiles.at(ROW).at(0).getHasPlayer())
    {
        tiles.at(ROW).at(0).removePlayer();
        tiles.at(ROW).at(1).setFocus(true);
        movePlayer(false);
    }
    else
    {
        for(int i = size - 1; i >= 1; i--)
        {
            if(tiles.at(ROW).at(i).getHasPlayer())//need to work on for players on the same column
            {
                tiles.at(ROW).at(i).removePlayer();
                tiles.at(ROW).at(i + 1).setFocus(true);
                movePlayer(false);
            }
        }
    }
    
}
void Board::slideFromAbove(const int ROW, const int COL, const int size, SDL_Renderer* renderer)
{
    for(int i = size - 1; i >= 1; i--)
    {
        if(i == size - 1) //setting buffer, storage and last tile
        {
            storage = buffer;
            
            buffer = tiles.at(size - 1).at(COL);
            tiles.at(size - 1).at(COL).removeBuffer();
            tiles.at(size - 1).at(COL).addBuffer(&buffer);
            tiles.at(size - 1).at(COL).setBufferBottom(true);
            buffer.setTileY(tiles.at(size - 1).at(COL).getTileY() + offsetY);
            buffer.setAngle(tiles.at(size - 1).at(COL).getAngle());
            tiles.at(ROW).at(COL).removeBuffer();
            
        }
        
        tiles.at(i).at(COL).setBot(tiles.at(i - 1).at(COL).getBot()); //setting tiles inbetween first and last
        tiles.at(i).at(COL).setTop(tiles.at(i - 1).at(COL).getTop());
        tiles.at(i).at(COL).setLeft(tiles.at(i - 1).at(COL).getLeft());
        tiles.at(i).at(COL).setRight(tiles.at(i - 1).at(COL).getRight());
        tiles.at(i).at(COL).setTexture(tiles.at(i - 1).at(COL).getTileTexStr().c_str(), renderer);
        tiles.at(i).at(COL).setAngle(tiles.at(i - 1).at(COL).getAngle());
        
        if(tiles.at(i - 1).at(COL).getTresTexStr() == "")
            tiles.at(i).at(COL).setTreasureTexture(NULL, renderer);
        else
            tiles.at(i).at(COL).setTreasureTexture(tiles.at(i - 1).at(COL).getTresTexStr().c_str(), renderer);
        
        if(i == 1) //setting first tile to storage
        {
            tiles.at(0).at(COL).setBot(storage.getBot());
            tiles.at(0).at(COL).setTop(storage.getTop());
            tiles.at(0).at(COL).setLeft(storage.getLeft());
            tiles.at(0).at(COL).setRight(storage.getRight());
            tiles.at(0).at(COL).setTexture(storage.getTileTexStr().c_str(), renderer);
            tiles.at(0).at(COL).setTreasureTexture(storage.getTresTexStr().c_str(), renderer);
            tiles.at(0).at(COL).setAngle(storage.getAngle());
            
        }
        
        
        
        render(renderer, true);
    }
    if(tiles.at(size - 1).at(COL).getHasPlayer()) //moving player(s)
    {
        tiles.at(size - 1).at(COL).removePlayer();
        tiles.at(0).at(COL).setFocus(true);
        movePlayer(false);
        
    }
    else if(tiles.at(0).at(COL).getHasPlayer())
    {
        tiles.at(0).at(COL).removePlayer(); 
        tiles.at(1).at(COL).setFocus(true);
        movePlayer(false);
    }
    else
    {
        for(int i = size - 1; i >= 1; i--)
        {
            if(tiles.at(i).at(COL).getHasPlayer())//need to work on for players on the same column
            {
                tiles.at(i).at(COL).removePlayer();
                tiles.at(i + 1).at(COL).setFocus(true);
                movePlayer(false);
            }
        }
    }
}
void Board::snapBufferToMouseGrid(bool inInitBoard, const SDL_Event* event)  //untested
{
    if(this -> size > 0)
    {
        if(inInitBoard) //setting the buffer tile's dimensions and position
        {
            
            for(int i = 0; i < tiles.size(); i++)
                for(int j = 0; j < tiles.at(0).size(); j++)
                {
                    if(tiles.at(i).at(j).getBufferIsAdjacent())
                    {
                        if(tiles.at(i).at(j).getBufferRight())
                            buffer.setTileX(tiles.at(i).at(j).getTileX() + offsetX);
                        else if(tiles.at(i).at(j).getBufferLeft())
                            buffer.setTileX(tiles.at(i).at(j).getTileX() - offsetX);
                        else
                            buffer.setTileX(tiles.at(i).at(j).getTileX());
                        
                        if(tiles.at(i).at(j).getBufferTop())
                            buffer.setTileY(tiles.at(i).at(j).getTileY() - offsetY);
                        else if(tiles.at(i).at(j).getBufferBottom())
                            buffer.setTileY(tiles.at(i).at(j).getTileY() + offsetY);
                        else
                            buffer.setTileY(tiles.at(i).at(j).getTileY());
                    }
                }

        }
        else if(event != NULL) //setting buffer tile position based on mouse click point input, snapping it to the grid. Untested
        {
            const int mouseY = event -> button.y;
            const int mouseX = event -> button.x;
            
            
            if(((mouseX < tiles.at(0).at(0).getTileX() || mouseX > tiles.at(size - 1).at(size - 1).getTileX() + offsetX))  && (mouseY < tiles.at(0).at(0).getTileY() || mouseY > tiles.at(size - 1).at(size - 1).getTileY() + offsetY))
            {
                
            } //do nothing if any corner area is clicked
            else
            {
                if(mouseX < tiles.at(0).at(0).getTileX())
                {
                    buffer.setTileX(0);
                    buffer.setCol(tiles.at(0).at(0).getCol());
       
                }
                else if(mouseX > (tiles.at(0).at(this -> size - 1).getTileX() + tiles.at(0).at(0).getTileX()))
                {
                    
                    buffer.setTileX(tiles.at(this -> size - 1).at(this -> size - 1).getTileX() + tiles.at(0).at(0).getTileX());
                        tiles.at(0).at(0).addBuffer(&buffer);
                }
                else
                {
                    for(int col = 0; col < this -> size; col++)
                    {
                          if(col == this -> size - 1)
                          {
                              if((mouseX > tiles.at(0).at(col).getTileX()) && mouseX <= (tiles.at(0).at(col).getTileX() + offsetX))
                                  buffer.setTileX(tiles.at(0).at(col).getTileX());
                          }
                        else if(mouseX >= tiles.at(0).at(col).getTileX() && mouseX <= tiles.at(0).at(col + 1).getTileX())
                            buffer.setTileX(tiles.at(0).at(col).getTileX());
                    }
                }
            
                if(mouseY < tiles.at(0).at(0).getTileY())
                {
                    buffer.setTileY(0);
                    buffer.setRow(tiles.at(0).at(0).getRow());
                }
                else if(mouseY > (tiles.at(this -> size - 1).at(0).getTileY() + tiles.at(0).at(0).getTileY()))
                {
                    buffer.setTileY(tiles.at(this -> size - 1).at(0).getTileY() + tiles.at(0).at(0).getTileY());
                }
                else
                {
                    for(int row = 0; row < this -> size; row++)
                    {
                        if(row == this -> size - 1)
                        {
                            if((mouseY >= tiles.at(this -> size - 1).at(0).getTileY()) && mouseY <= (tiles.at(this -> size - 1).at(0).getTileY() + tiles.at(0).at(0).getTileY()))
                                buffer.setTileY(tiles.at(row).at(0).getTileY());
                        }
                        else if(mouseY >= tiles.at(row).at(0).getTileY() && mouseY <= tiles.at(row + 1).at(0).getTileY())
                            buffer.setTileY(tiles.at(row).at(0).getTileY());
                    }
                }
            buffer.setTileHeight(tiles.at(0).at(0).getRect() -> h);
            buffer.setTileWidth(tiles.at(0).at(0).getRect() -> w);
            
            for(int i = 0; i < tiles.size(); i++)
                for(int j = 0; j < tiles.at(0).size(); j++)
                {
                    tiles.at(i).at(j).removeBuffer();
                    if(buffer.getTileX() - offsetX == tiles.at(i).at(j).getTileX()) //checking rightmost board flank. checks COLUMNS
                    {
                        if(buffer.getTileY() == tiles.at(i).at(j).getTileY()) //checking ROW
                        {
                            tiles.at(i).at(j).setBufferRight(true);
                            tiles.at(i).at(j).addBuffer(&buffer);
                            buffer.setCol(tiles.at(i).at(j).getCol());
                            buffer.setRow(tiles.at(i).at(j).getRow());
                        }
                    }
                    else if(buffer.getTileX() + offsetX == tiles.at(i).at(j).getTileX()) //checking leftmost board flank. checks COLUMNS
                    {
                        if(buffer.getTileY() == tiles.at(i).at(j).getTileY()) //checking ROW
                        {
                            tiles.at(i).at(j).setBufferLeft(true);
                            tiles.at(i).at(j).addBuffer(&buffer);
                            buffer.setCol(tiles.at(i).at(j).getCol());
                            buffer.setRow(tiles.at(i).at(j).getRow());
                            
                        }
                    }
                    else if(buffer.getTileX() == tiles.at(i).at(j).getTileX()) // checks COLUMNS
                    {
                        if(buffer.getTileY() + offsetY == tiles.at(i).at(j).getTileY()) //on top of the game board. checks ROW
                        {
                            tiles.at(i).at(j).setBufferTop(true);
                            tiles.at(i).at(j).addBuffer(&buffer);
                            buffer.setCol(tiles.at(i).at(j).getCol());
                            buffer.setRow(tiles.at(i).at(j).getRow());
                            
                        }
                        else if(buffer.getTileY() - offsetY == tiles.at(i).at(j).getTileY()) //underneath game board. checks ROW
                        {
                            tiles.at(i).at(j).setBufferBottom(true);
                            tiles.at(i).at(j).addBuffer(&buffer);
                            buffer.setCol(tiles.at(i).at(j).getCol());
                            buffer.setRow(tiles.at(i).at(j).getRow());
                            
                        }
                    }
                }
            
        }
    }
    }
        
}

Board::~Board()
{
    if(boardTexture != NULL)
    {
        SDL_DestroyTexture(boardTexture);
        boardTexture = NULL;
    }
}
//end of davids function members

