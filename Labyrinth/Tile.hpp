/*
 Rufat Hajizada
 Tile class for setting and initializing tiles
 09/25/2017 11:47
 Last augmented by Marcus Galeotti on 11/27/17
 */

#ifndef TILE_HPP
#define TILE_HPP

#include <stdio.h>
#include <set>
#include <vector>
#include <stdexcept>
#include <string>
#include <sstream>
#include "Tile.hpp"
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include "Treasure.hpp"

class Tile
{
    bool topCon = false; //con means connection
    bool rightCon = false;
    bool botCon = false;
    bool leftCon = false;
    bool hasPlayer = false;
    char treasureChar = ' ';
    short int whichPlayer = -1; //this is set to -1 by default to indicate that the tile has no player on it
    bool isFixed = false;
    bool hasFocus = false; //gains focus if clicked through checkIfClicked function
    int row = -1, col = -1;
    int angle = 0; //can only be 0, 90, 180, or 270
    bool bufferIsAdjacent = false;
    bool bufferIsLeft = false;
    bool bufferIsRight = false;
    bool bufferIsBottom = false;
    bool bufferIsTop = false;
    std::string treasureTextureStr = "";
    std::string tileTextureStr = "";
    
    Uint32 mouseFocusButton = 0;
    
    SDL_Rect tileRect;
    SDL_Texture* tileTexture = NULL;
    SDL_Texture* treasureTexture = NULL;
    Tile* buffer = NULL;
    
    
    //start of rufat's private functions
    int conNum(){
        // returns number of connections that tile has for loading textures
        int n = 0; // number of connections
        if((leftCon == true && rightCon == true && topCon == false && botCon ==false) || (topCon == true && botCon ==true && leftCon == false && rightCon == false)){
            n = 1;
        }
        else{
            bool conArray[4] = {topCon, rightCon, leftCon, botCon}; // Array of connections
            int i =0;
            for (i = 0; i < 4; i++){
                if(conArray[i] == true){
                    n++;
                }
            }
        }
        return n;
    }
    
    std::string pathInit()
    {
        using namespace std;
        
        stringstream pathSstr;
        pathSstr << "tiles/" << conNum() << ".jpeg";
       // string path ("tiles/" + conNum() + ".jpeg"); // fixed the problem;
        
        return pathSstr.str(); // now returns the string
    }
    //end of rufat's private functions
    
    
public:
    
    //start of davids data members
    std::set<int> connections;
    //end of davids data members
    
    std::string getTresTexStr()
    {
        return treasureTextureStr;
    }
    std::string getTileTexStr()
    {
        return tileTextureStr;
    }
    void setTresTexStr(std::string tresStr)
    {
        treasureTextureStr = tresStr;
    }
    void setTileTexStr(std::string tileStr)
    {
        tileTextureStr = tileStr;
    }
    Tile(const int x, const int y, const int width, const int height)
    {
        tileRect.x = x;
        tileRect.y = y;
        tileRect.w = width;
        tileRect.h = height;
    }
    Tile(int tileX = 0, int tileY = 0, int tileWidth = 0, int tileHeight = 0, int rowVal = -1, int colVal = -1, bool topConVal = false, bool rightConVal = false, bool bottomConVal = false, bool leftConVal = false, bool hasPlayerVal = false,bool isFixedVal = false, char treasureCharVal = ' ', int whichPlayerVal = -1, bool hasFocusVal = false):  topCon(topConVal), rightCon(rightConVal),botCon(bottomConVal), leftCon(leftConVal),hasPlayer(hasPlayerVal),isFixed(isFixedVal), treasureChar(treasureCharVal), whichPlayer(whichPlayerVal), hasFocus(hasFocusVal), row(rowVal), col(colVal)
    {
        tileRect.x = tileX;
        tileRect.y = tileY;
        tileRect.w = tileWidth;
        tileRect.h = tileHeight;
    }
     /* buttonVal:  one of the macros defined by the SDL library for event.button.button event values */
    void setFocusButton(const Uint32 buttonVal)
    {
        mouseFocusButton = buttonVal;
    }
    double getAngle()
    {
        return angle;
    }
    bool getBufferIsAdjacent()
    {
        return bufferIsAdjacent;
    }
    void resetBufferValsToFalse()
    {
        bufferIsAdjacent = false;
        bufferIsTop = false;
        bufferIsBottom = false;
        bufferIsLeft = false;
        bufferIsRight = false;
    }
    void removeTreasure()
    {
        SDL_DestroyTexture(treasureTexture);
        treasureTextureStr = "";
        treasureTexture = NULL;
    }
    bool getBufferTop()
    {
        return bufferIsTop;
    }
    bool getBufferBottom()
    {
        return bufferIsBottom;
    }
    bool getBufferLeft()
    {
        return bufferIsLeft;
    }
    bool getBufferRight()
    {
        return bufferIsRight;
    }
    void setBufferTop(bool top)
    {
        bufferIsTop = top;
    }
    void setBufferBottom(bool bottom)
    {
        bufferIsBottom = bottom;
    }
    void setBufferLeft(bool left)
    {
        bufferIsLeft = left;
    }
    void setBufferRight(bool right)
    {
        bufferIsRight = right;
    }
    void addBuffer(Tile* boardBufferTile)
    {
        buffer = boardBufferTile;
        bufferIsAdjacent = true;
    }
    void removeBuffer()
    {
        buffer = NULL; //not deleting buffer. Is still being used!
        resetBufferValsToFalse();
    }
    void setAngle(const int angleVal)
    {
        if(angleVal == 0 || angleVal == 90 || angleVal == 180 || angleVal == 270)
            angle = angleVal;
        else if(angleVal == 360)
            angle = 0;
    }
    /* Purpose: this function will set a tile's texture given a character string to its filepath
     Note: tileImg can only be NULL if another tile holds a reference to it's former texture, otherwise a memory leak may occur */
    SDL_bool setTexture(const char* tileImg, SDL_Renderer* renderer)//temporary measure
    {
        SDL_bool success = SDL_FALSE;
        SDL_Texture* texture;
        if(tileImg == NULL)
        {
            tileTexture = NULL; //being held by another tile, will be released by its destructor
            success = SDL_TRUE;
        }
        else if((texture = IMG_LoadTexture(renderer, tileImg)) != NULL)
        {
            tileTextureStr = tileImg;
            success = SDL_TRUE;
            tileTexture = texture;
            texture = NULL;
        }
        return success;
    }
    /* Purpose: this function will set a tile's treasure texture given a character string to its filepath
     Note: treasureImg can only be NULL if another tile holds a reference to it's former texture, otherwise a memory leak may occur */
    SDL_bool setTreasureTexture(const char* treasureImg, SDL_Renderer* renderer)//temporary measure
    {
        
        SDL_bool success = SDL_FALSE;
        SDL_Texture* texture;
        if(treasureImg == NULL || strcmp(treasureImg, "") == 0)
        {
            treasureTexture = NULL; //being held by another tile, will be released by its destructor
            treasureTextureStr = "";
            success = SDL_TRUE;
        }
        else if((texture = IMG_LoadTexture(renderer, treasureImg)) != NULL)
        {
            treasureTextureStr = treasureImg;
            success = SDL_TRUE;
            treasureTexture = texture;
            texture = NULL;
        }
        return success;
    }
    
    SDL_Rect* getRect()
    {
        return &tileRect;
    }
     void setCol(const int colVal)
     {
         if(colVal >= 0)
             col = colVal;
     }
     void setRow(const int rowVal)
     {
         if(rowVal >= 0)
             row = rowVal;
     }
     int getRow()
     {
         return row;
     }
     int getCol()
     {
         return col;
     }
    /* Purpose: to get the x screen coordinate of the tile this is called on */
    int getTileX()
    {
        return tileRect.x;
    }
    bool getHasPlayer()
    {
        return hasPlayer;
    }
    /* Purpose: to return the y screen coordinate of the tile this is called on */
    int getTileY()
    {
        return tileRect.y;
    }
    /* Purpose: to set the x screen coordinate of the tile this is called on */
    void setTileX(const int x)
    {
        tileRect.x = x;
    }
    /* Purpose: to set the y screen coordinate of the tile this is called on */
    void setTileY(const int y)
    {
        tileRect.y = y;
    }
    /* Purpose: to set the width, in screen coordinates, of the tile this is called on */
    void setTileWidth(const int width)
    {
        tileRect.w = width;
    }
    /* Purpose: to set the height, in screen coordinates, of the tile this is called on */
    void setTileHeight(const int height)
    {
        tileRect.h = height;
    }
    /* Purpose: to get which player resides on this tile, if any.
     It returns -1 to indicate the tile does not have a player on it */
    short int getWhichPlayer()
    {
        return whichPlayer;
    }
    
    /*void setTileVals(bool topConVal, bool rightConVal,bool bottomConVal, bool leftConVal, bool hasPlayerVal,char treasureChar, int  whichPlayerVal, int tileWidth, int tileHeight, int tileX = 0, int tileY = 0)
    {
        topCon = topConVal;
        leftCon = leftConVal;
        rightCon = rightConVal;
        botCon = bottomConVal;
        
    }*/
    void rotate(void)
    {
        
        bool temp = topCon;
        topCon = leftCon;
        leftCon = botCon;
        botCon = rightCon;
        rightCon = temp;
        
        int tileWidth = this -> tileRect.w;
        tileRect.w = tileRect.h;
        tileRect.h = tileWidth;
        
        this -> setAngle(angle + 90);
    }
    
    void setFix(bool fixValue){
        isFixed = fixValue;
    }
    void setPlayer(int person){
        if(person >= 0)
        {
            hasPlayer = true;
            whichPlayer = person;
        }
        
    }
    /* Purpose: to remove a player and set it's 'hasPlayer' value to false */
    void removePlayer()
    {
        whichPlayer = -1;
        hasPlayer = false;
    }
    
    void setTreasure(char treasureCharacter){
        treasureChar = treasureCharacter;
    }
    
    void setTop(bool top){
        topCon = top;
    }
    void setBot(bool bot){
        botCon = bot;
    }
    
    void setLeft(bool left){
        leftCon = left;
    }
    
    void setRight(bool right){
        rightCon = right;
    }
    
    bool getTop(){
        return topCon;
    }
    bool getBot(){
        return botCon;
    }
    
    bool getLeft(){
        return leftCon;
    }
    
    bool getRight(){
        return rightCon;
    }
    
    bool getFixed(){
        return isFixed;
    }
    void setFocus(bool hasFocusVal)
    {
        hasFocus = hasFocusVal;
    }
    bool getFocus()
    {
        return hasFocus;
    }
    SDL_Texture* getTexture()
    {
        return tileTexture;
    }
    SDL_Texture* getTreasureTexture()
    {
        return treasureTexture;
    }
    
    //start of rufat's public functions
    void setTexture(SDL_Renderer* renderer)
    // Function that sets the texture for the tile
    {
        SDL_Texture * tempTexture = IMG_LoadTexture(renderer, pathInit().c_str());
        if(tempTexture == NULL )
        {
            throw std::runtime_error("IMG_load failed: ");
        }
        tileTexture = tempTexture;
        tempTexture = NULL;
    }
    //end of rufat's public functions
    ~Tile()
    {
        if(tileTexture != NULL)
        {
            SDL_DestroyTexture(tileTexture);
            SDL_DestroyTexture(treasureTexture);
            tileTexture = NULL;
            treasureTexture = NULL;
        }
        
    }
};
#endif /* TILE.HPP */


