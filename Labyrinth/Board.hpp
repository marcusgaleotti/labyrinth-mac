#ifndef Board_hpp
#define Board_hpp

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <SDL2_ttf/SDL_ttf.h>
#include "Tile.hpp"
#include <vector>
#include <exception>
#include <stdexcept>	
#include <iostream>
#include <unistd.h>
#include <algorithm>
#include <sstream>
#include <cmath>
#include "bufferTile.hpp"
#include <set>
#include "Player.hpp"
#include <cstdlib>
#include <ctime>
/*#include "Soundclass/Slide1Sound/Sound.h"
#include "Soundclass/Slide1Sound/stdafx.h"
#include "Soundclass/Slide1Sound/targetver.h"*/
/*
 Author: Marcus Galeotti
 Purpose: Board class that represents the whole game board. To be used with a tile and player class.
 Date of last update: 12/01/2017
*/
class Board
{
private:
    std::vector<std::vector<Tile>> tiles;
    std::vector<Player> players;
    int size; //size of one ROW or COLUMN of the NxN game board
    
    int fixedCornerWithTreasr = 4; //internal board state data. Defaults are for 7x7
    int fixedTeeWithTreasr = 12;
    int straight = 12;
    int cornerUnfixedNoTresr = 10;
    int cornerUnfixedWithTresr = 6;
    int TeeUnfixedWithTreasr = 8;
    int fixedTilesTotal = 16;
    int unfixedTilesTotal = 34;
    
    int numOfPlayers = 1;
    bool firstInit; //variable to indicate if the board has been initialized at least once or not
    int offsetX = 0; //same as tileWidth and tileHeight
    int offsetY = 0;
    SDL_Texture* boardTexture;
    
    
    // start of david's data members
    Tile buffer;
    Tile storage;
    //end of david's data members

    /* Purpose: this function determines what mouse button was recently clicked (left, middle or right),
     and whether the tile is clicked.
     1. If the tile was not recently clicked, it returns -1
     2. If the tile was recently clicked, and it was recently clicked with the left mouse button,
     it returns SDL_BUTTON_LEFT
     3. If the tile was recently clicked, and it was recently clicked with the middle mouse button, it returns SDL_BUTTON_MIDDLE
     4. If the tile was recently clicked, and it was clicked with the right mouse button,
     it returns SDL_BUTTON_RIGHT
     5. If some mouse button other than left, middle, or right is pressed and the mouse was recently clicked within the tile area, this function returns -2
     6. If tileRect is NULL, -3 is returned.
     - negative values are returned to ensure that they aren't mixed up on some platform with the other
         defined SDL button constants. This can be ensured since the button constants are all documented as being of type Uint8
     - tileRect must be valid in order for this function to do anything other than return 5, meaning it cannot be null and must have been initialized properly.
     6. Any other values returned from this function are invalid and should be considered an error
     
     tileRect - the SDL_Rect pointer associated with the tile to be tested */
     short int clicked(const SDL_Event event, const SDL_Rect* tileRect);

    /* Purpose: this function establishes the tile counts for each type by initializing the appropriate private data members of this object for 7x7 board only at this time. It is used only internally within the class
        - this function requires that the size variable in the Board class be initialized with a value before being called */
    void setTileCounts()
    {
        switch(this -> size)
        {
            case 7:
                fixedTeeWithTreasr = 12;
                fixedCornerWithTreasr = 4;
                straight = 12;
                cornerUnfixedNoTresr = 10;
                cornerUnfixedWithTresr = 6;
                TeeUnfixedWithTreasr = 6;
                
                fixedTilesTotal = 16;
                unfixedTilesTotal = 25;
                break;
                
            default:
                fixedTeeWithTreasr = 0;
                fixedCornerWithTreasr = 0;
                straight = 0;
                cornerUnfixedNoTresr = 0;
                cornerUnfixedWithTresr = 0;
                TeeUnfixedWithTreasr = 0;
                break;
        }
    }

    
    /* Purpose: this function performs sliding from above
     size - the board size from the board object this is called from */
    void slideFromAbove(const int ROW, const int COL, const int size, SDL_Renderer* renderer);
    /* Purpose: this function performs sliding from the left
     size - the board size from the board object this is called from */
    void slideFromLeft(const int ROW, const int COL, const int size, SDL_Renderer* renderer);
    /* Purpose: this function performs sliding from the right
     size - the board size from the board object this is called from */
    void slideFromRight(const int ROW, const int COL, const int size, SDL_Renderer* renderer);
    /* Purpose: this function performs sliding from the bottom
     size - the board size from the board object this is called from */
    void slideFromBottom(const int ROW, const int COL, const int size, SDL_Renderer* renderer);
public:
    /* Purpose: Initializes the Board object container for tiles based on the window width and height
     of the provided window
     firstInitVal: a boolean value to pass to initGameBoard to indicate if it is the first initialization. Used to set initial
                     player board positions. It is by default true in the constructor because the constructor calls the first
                     initialization if using a Board object. It is set to false just before the constructor exits
     */
    Board(const int sizeOfBoard, SDL_Window* window, SDL_Renderer* renderer, const int numberOfPlayers = 1, const bool firstInitVal = true);
    std::vector<std::vector<Tile>> getGameBoard()
    {
        return tiles;
    }
    int getBoardSize()
    {
        return size;
    }
    void setBoardSize(int newSize)
    {
        size = newSize;
    }
    Tile* getBufferTile()
    {
        return &buffer;
    }
    Tile* getStorageTile()
    {
        return &storage;
    }
    void setBufferTile(const Tile newBuf)
    {
        buffer = newBuf;
    }
    void setStorageTile(const Tile newStor)
    {
        storage = newStor;
    }
    std::vector<Player> getPlayerList()
    {
        return players;
    }
    /* Purpose: this function sets the texture for the background of the game board */
    SDL_bool setTexture(const char* tileImg, SDL_Renderer* renderer);
    /* Purpose: this function encapsulates the sliding procedure in the game */
    void slide(SDL_Renderer*);
    /* Purpose: this function sets the position in tileNoBuffer that corresponds with the current value of tileClickState.
     Specifically:
     - tileNoBuffer[0] corresponds to tileClickState 1.
     - tileNoBuffer[1] corresponds to tileClickState 2.
     if tileClickState is in any other position, tileNoBuffer is unmodified. This function modifies tileNoBuffer otherwise, and
     the position corresponding to tileClickState is modified. */
    void setTileNoBuffer(const int tileNo);;
    
    /* Purpose: Function to be called just before and in event handling loop to update window
     (re-render) when things change or when events occur
     renderer : a properly initalized SDL_Renderer pointer initalized by a sucessful call to SDL_CreateRenderer or equivalent */
   // void render(SDL_Renderer* renderer);
    
    /* Purpose: Function to be called just before and in event handling loop to update window
     (re-render) when things change or when events occur.
         - This version can perform rotation in degrees, texture clipping, and image flipping about the center variable
         - clipping is currently not implemented
     renderer : a properly initalized SDL_Renderer pointer initalized by a sucessful call to SDL_CreateRenderer or equivalent
     clip: an SDL_Rect with dimensions mapping to the dimensions of a section of a sprite sheet
             to be used as the texture value for the final, outputted tile
     angle: the angle, as a real number, to rotate the image.
         - The value must be in degrees
         - The rotation occurs in a clockwise direction with the specified angle
     center: the point that the outputted tile is to be rotated around, if angle is not equal to 0
         - rotation occurs about the center of the outputted tile if center is null
     flip: the defined SDL enumerated flip values to indicate if flipping is to be done
         - flipping can be done vertically or horizontally
     renderingTextures: a boolean indicating if textures are being used to render
         - if not specified, or false is passed in for it, it will use drawing colors to
             render tiles instead */
    void render(SDL_Renderer* renderer, const bool renderingTextures = false, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);
    
    
    
    /* Purpose: this function is called once per game cycle to refresh the game window display and upon certain game events occuring
     window : a properly initialized SDL_Window pointer initialized with a sucessful call to SDL_CreateWindow or equivalent
     renderer : a properly initialized SDL_Renderer pointer initialized with a sucessful call to SDL_CreateRenderer or equivalent*/
    void initGameBoard(SDL_Window* window, SDL_Renderer* renderer = NULL);
    
    /*initializes window, renderer, all of SDL'S base components, SDL2_image, and SDL2_ttf */
    static bool initSDL(int IMG_INIT_FLAGS, Uint32 SDL_INIT_FLAGS, int width, int height, Uint32 window_flags, SDL_Window** window, SDL_Renderer** renderer);
    
    static void displaySDLError(std::ostream &os, const std::string &msg);
    
    /* Purpose: this function performs one pass of an event handling routine, checking for and performing behaviors based on whether certain events have occurred. This function blocks until
     an event has occurred and will block indefinitely if one does not occur.
         - this may change in the future, as their is a timed blocking function that can be used if
             other things need to be done. Alternately, multithreading may be something we can take advantage of.
     Board* - a valid board object
     SDL_Window* - a valid, initialized SDL_Window* that has been initialized through one of the
     window creating functions provided by SDL
     SDL_Renderer* - a valid, initialized SDL_Renderer that has been initialized through one of the
     renderer creating functions provided by SDL */
    int handleEvents(Board*, SDL_Window*, SDL_Renderer*);
    
    /* Purpose: this function checks if any tile was clicked on the game board.
     - If it was clicked, this function returns the tile number that was clicked.
     - If no tile was clicked, this function returns a -1.
     
     The value returned is zero based; that means that the first tile is tile 0 and the last tile is numberOfTiles - 1, and the range of tiles is [0, numberOfTiles - 1] (interval notation)
     
     sdlEvent: an SDL_Event that has been declared and initialized. It only makes sense to pass in an event after it has been filled in when an event has actually occurred (through SDL_PollEvent for instance), but that is not a requirement
     
      when called on a given Board instance, this function sets the focus variable to true for the tile that is clicked if and only if one was clicked, and resets all others to false each time this function is called */
    int checkIfClicked(const SDL_Event sdlEvent);
    
    /* Purpose: this function encapsulates and abstracts the process of moving a player. This function now
        sets the tile which has focus when it is called, if one has the focus, to false.
     tileNo - the result of calling Board::checkIfClicked on a specific board instance
     clicking - passed a true value if being called when clicking, and false if being called from a sliding function */
    void movePlayer(const bool clicking);
    
    /* Purpose: this function encapsulates the process of snapping the buffer tile to the grid pattern of the board based on
        left clicking outside of the game board. If the size of the board is less than or equal to zero, the buffer tile is unchanged
     If inInitBoard is true, this function simply takes care of the proper resizing and dimensions/position of the buffer tile without changing it's position relative to other tiles in the game board
     - inInitBoard: set it to true if being called from the initBoard function, and false otherwise
     - event: Set to NULL if called from initGameBoard, and set to the address of an SDL_Event with event information populated into it via an SDL_PollEvent call or equivalent */
    void snapBufferToMouseGrid(const bool inInitBoard, const SDL_Event* event);
    
    //begin of david's function members
    /*
     * Pass a tile and an integer corresponding to a direction.
     * Currently uses an arbitrary mapping of integers to directions which could be passed based on which tile is selected.
     * 0 - up
     * 1 - right
     * 2 - down
     * 3 - left
     */
    bool check_connection(Tile cur_tile, int dir);
    
    /*
     * Alters the position of each tile object for a specific row or column, the tile being pushed off is placed in storage,
     * the other tiles move over by one, and the buffer tile is added on. Finally, the storage tile is set as the new buffer.
     * Direction variable for col is 0 for up and 1 for down
     * Direction variable is row 0 for left and 1 for right
     */
    bool slide_row(int r, bool dir);
    bool slide_col(int c, bool dir);
    /*
     * Populates the set of connected tiles of the current tile
     * Formula for addition to the set:
     *   Value = (Board size)x + y
     * Note: Value does not need to be calculated in reverse, as the function that checks if two tiles are connected
     * will simply calculate the value again to determine whether it is part of the first tile's set
     */
    std::set<int> find_connections(Board cur_board, Tile cur_tile);
    /*
     * Overloaded version of above function. Same, except it will not check in the direction specified.
     * This prevents an infinite loop of checking between two adjacent, connected tiles, as once it finds a connection and recurses,
     * it would check back in the direction it came from, which is guaranteed to be a true connection.
     */
    std::set<int> find_connections(Board cur_board, Tile cur_tile, int null_dir);
    
    /*
     * Perform the set calculation to determine whether the target tile is a member of the current tiles connections set
     * Returns true if it finds the number in the current tile's set, and false otherwise
     */
    bool commit_connection (Tile cur_tile, Tile target_tile);
    //end of david's function members
    ~Board();
    

    
};


#endif /* Board_hpp */
