/*
 Rufat Hajizada
 Treasure class for treaure initializing and texture loading
 11/13/2017
 */

#ifndef TREASURE_HPP
#define TREASURE_HPP

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include "Tile.hpp"
//using namespace std;

class Treasure 
{
    SDL_Rect treasureRect;
    SDL_Texture* texture = NULL;
    int figure;
};




#endif /* TREASURE.HPP */
