/*
 Labyrinth header for macOS
 stores all the include files for the project
 */

#ifndef Labyrinth_h
#define Labyrinth_h
#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2_image/SDL_image.h>
#include <SDL2_ttf/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <exception>
#include <stdexcept>
#include "Tile.hpp"
#include "Board.hpp"

#endif /* Labyrinth_h */
