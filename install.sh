sudo mkdir /Library/Frameworks
sudo cp Frameworks/SDL2_image.framework /Library/Frameworks
sudo cp Frameworks/SDL2_ttf.framework /Library/Frameworks
sudo cp Frameworks/SDL2.framework /Library/Frameworks
echo "SDL installed on your system"
mkdir /Users/Shared/Labyrinth
mkdir /Users/Shared/Labyrinth/tiles
mkdir /Users/Shared/Labyrinth/players
mkdir /Users/Shared/Labyrinth/bg
mkdir /Users/Shared/Labyrinth/treasure
cp Labyrinth/tiles/1.jpg /Users/Shared/Labyrinth/tiles
cp Labyrinth/tiles/2.jpg /Users/Shared/Labyrinth/tiles
cp Labyrinth/tiles/3.jpg /Users/Shared/Labyrinth/tiles
cp Labyrinth/players/p1.png /Users/Shared/Labyrinth/players
cp Labyrinth/players/p2.png /Users/Shared/Labyrinth/players
cp Labyrinth/players/p3.png /Users/Shared/Labyrinth/players
cp Labyrinth/players/p4.png /Users/Shared/Labyrinth/players
cp Labyrinth/bg/bg.jpeg /Users/Shared/Labyrinth/bg
cp Labyrinth/treasure/1.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/2.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/3.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/4.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/5.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/6.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/7.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/8.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/9.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/10.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/11.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/12.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/13.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/14.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/15.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/16.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/17.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/18.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/19.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/20.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/21.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/22.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/23.png /Users/Shared/Labyrinth/treasure
cp Labyrinth/treasure/24.png /Users/Shared/Labyrinth/treasure
echo "All files moved to /Users/Shared/Labyrinth/"
sudo xcode-select -s /Applications/Xcode.app/Contents/Developer
xcodebuild
mkdir Labyrinth.app
mkdir Labyrinth.app/Contents
mkdir Labyrinth.app/Contents/MacOS
mkdir Labyrinth.app/Resources
cp build/Release/Labyrinth Labyrinth.app/Contents/MacOS
cp App/Info.plist Labyrinth.app/
cp App/icon.icns Labyrinth.app/Resources/